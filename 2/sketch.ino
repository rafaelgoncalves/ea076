/*
Projeto 2
Rafael Gonçalves (RA: 186062)
Thais Del Picchia (RA:206043)
*/

// Importação de bibliotecas
#include <LiquidCrystal.h>
#include <Wire.h>

// Estados do motor
enum motorStates {
  STOP, // parado
  CW,   // sentido horário
  CCW,  // sentido anti-horário
};

// Qual display está aceso
enum displayStates {
  ONE   = 0b11100000,
  TWO   = 0b11010000,
  THREE = 0b10110000,
  FOUR  = 0b01110000,
};

// Digitos a serem mostrados no display
int displayValues[4];

// Flags
bool decode = false;
bool setMotor = false;

// Pinos
const int ENCODER_PIN = 2;
const int PWM_PIN = 3;
const int MOTOR_PINS[] = {4,5};
const int RS = 12;
const int E = 11;
const int DATA[] = {6,7,8,9};
const int DISPLAY_VEC_LEN = 4;

// LCD
//Definir os pinos do arduino conectados ao LCD, sendo a ordem:
// (rs, enable, entrada de dados)
LiquidCrystal lcd(RS,E,DATA[0],DATA[1],DATA[2],DATA[3]);

// String de comando
String cmdString = "";
String msg = "";

// Contador para calcular a velocidade do motor
volatile unsigned int obstructionCounter = 0;

// Variável para registro de tempo
volatile unsigned long elapsedTime = 0;

// Constantes para os cálculos de velocidade
const int SPEED_CALCULATION_PERIOD = 600;
const int PULSES_PER_ROTATION = 2;

// Variável com velocidade atual do motor
unsigned int currentSpeed = 0;

// Variável que diz se há uma estimativa para a velocidade
bool hasSpeedEstimative = false;

// Variável com direção do motor
int motorDirection = STOP;

// Variável com string da velocidade selecionada
String vel = "";

// Variável com número da velocidade selecionada
unsigned int velNumber = 0;

// Expansor de portas
const int TWI_ADDR = 0x20;

// Configuração Timer0
void configTimer0(){
  TCCR0A = 0x02;
  OCR0A = 124;
  TIMSK0 = 0x02;
  TCCR0B = 0x05;
}

// Rotina de servico de interrupcao do temporizador
ISR(TIMER0_COMPA_vect){
  elapsedTime += 8;
}

// Conta obstruções no encoder óptico
void doCountObstruct() {
  obstructionCounter++;
};

// Atualiza vetor utilizado pela função que ativa displays de 7-seg com
// codificação BCD da estimativa da velocidade
void doChangeSpeedEstimative() {
  displayValues[3] = currentSpeed % 10;
  displayValues[2] = (currentSpeed/10) % 10;
  displayValues[1] = (currentSpeed/100) % 10;
  displayValues[0] = (currentSpeed/1000) % 10;
}

// Calcula velocidade efetiva estimada
void doCalculateSpeed() {
  // velocidade = (numero de voltas)/(tempo em min) =
  //            = (número de pulsos/pulsos por rotação)/(tempo em ms/60000) =
  //            = (60000*número de pulsos)/(pulsos por rotação*tempo em ms)
  currentSpeed = (60000*obstructionCounter)/(PULSES_PER_ROTATION*elapsedTime);
  obstructionCounter = 0;
  elapsedTime = 0;
  hasSpeedEstimative = true;
};

// Função para mostrar valor nos displays de 7-seg usando mecanismo de
// multiplexação com ciclo total menor do que 1/30s
void doSetDisplays() {
    // elapsedTime / 8 => intervalos de 8ms
    // ciclos de 4*8 = 32ms < 1/30 s
    // (elapsedTime/8) % 4)
    switch ((elapsedTime>>3) & 0b00000011) {
      case 0:
        setDisplay(displayValues[0], ONE);
        break;
      case 1:
        setDisplay(displayValues[1], TWO);
        break;
      case 2:
        setDisplay(displayValues[2], THREE);
        break;
      case 3:
        setDisplay(displayValues[3], FOUR);
        break;
    }
}

// Função para atualizar display (display) com um certo dígito (digit)
void setDisplay(int  digit, int display) {
    byte data = 0x00;
    data = display | (digit&0x0F);
    Wire.beginTransmission(TWI_ADDR);
    Wire.write(data);
    Wire.endTransmission();
}

// Função para limpar display de 7-seg (usado apenas para debug)
void clearDisplays() {
    Wire.beginTransmission(TWI_ADDR);
    Wire.write(0xFF);
    Wire.endTransmission();
}

// Toma ações em relação a mensagem de comando recebida via interface serial
void doDecode() {
  if(cmdString.startsWith("VEL ")){
    vel = cmdString.substring(4,7);
    if(vel == "*"){
        Serial.println("ERRO: PARAMETRO AUSENTE");
        decode = false;
        cmdString = "";
        return;
    } else if(cmdString.length() != 8){
        Serial.println("ERRO: PARAMETRO INCORRETO");
        decode = false;
        cmdString = "";
        return;
    } else if(isDigit(vel[0]) && isDigit(vel[1]) && isDigit(vel[2])){
      velNumber = vel.toInt();
        if(velNumber > 100 || velNumber < 0){
            Serial.println("ERRO: PARAMETRO INCORRETO");
            decode = false;
            cmdString = "";
            return;
        }
        msg = "OK VEL ";
        setMotor = true;
        msg.concat(vel);
        msg.concat("%");
        Serial.println(msg);
        decode = false;
        cmdString = "";
        return;
    } else {
        Serial.println("ERRO: PARAMETRO INCORRETO");
        decode = false;
        cmdString = "";
        return;
    }
  }
    else if(cmdString.startsWith("VENT") && cmdString.length() == 5){
        // Previnir mudança brusca de sentido de rotação do motor
        if (currentSpeed == 0) {
            Serial.println("OK VENT");
            setMotor = true;
            decode = false;
            motorDirection = CW;
            cmdString = "";
        }
        else {
            setMotor = true;
            motorDirection = STOP;
        }
        return;
    }else if(cmdString.startsWith("EXAUST") && cmdString.length() == 7){
        // Previnir mudança brusca de sentido de rotação do motor
        if (currentSpeed == 0) {
            Serial.println("OK EXAUST");
            setMotor = true;
            motorDirection = CCW;
            decode = false;
            cmdString = "";
        }
        else {
            setMotor = true;
            motorDirection = STOP;
        }
        return;
    }else if(cmdString.startsWith("PARA")&& cmdString.length() == 5){
        Serial.println("OK PARA");
        setMotor = true;
        decode = false;
        motorDirection = STOP;
        cmdString = "";
        return;
    }else if(cmdString.startsWith("RETVEL")&& cmdString.length() == 7){
        Serial.println("VEL: ");
        Serial.print(currentSpeed);
        Serial.println(" RPM");
        decode = false;
        cmdString = "";
        return;
    }else{
        Serial.println("ERRO: COMANDO INEXISTENTE");
        decode = false;
        cmdString = "";
        return;
    }
}

// Função que le um caracter por ciclo de vida
void doReadOneChar() {
  char inputChar = Serial.read();
  if (inputChar != '\n'){
    cmdString += inputChar;
  }
  if (inputChar == '*')
      decode = true;
}

// Imprime estado do sistema no LCD
void doUpdateLCD() {
  lcd.clear();
  lcd.setCursor(0,0);
  if(motorDirection == CW){
    lcd.print("VENTILADOR");
    }else if(motorDirection == CCW){
    lcd.print("EXAUSTOR");
    }else if(motorDirection == STOP){
    lcd.print("PARADO");
    }
  lcd.setCursor(0, 1);
  lcd.print(velNumber);
  lcd.setCursor(4, 1);
  lcd.print("%");
}

// Ajusta velocidade do motor na saída PWM
// Ajusta sentido de rotação
void doSetMotor() {
  if (motorDirection == STOP) {
    analogWrite(PWM_PIN, 0);
  } else {
    analogWrite(PWM_PIN, map(velNumber, 0, 100, 0, 255));
    if (motorDirection == CW) {
      digitalWrite(MOTOR_PINS[0], HIGH);
      digitalWrite(MOTOR_PINS[1], LOW);
    }
    else {
      digitalWrite(MOTOR_PINS[0], LOW);
      digitalWrite(MOTOR_PINS[1], HIGH);
    }
  }
  setMotor = false;
}

// Configurações iniciais
void setup()
{
  // Configura GPIOs como saída ou entrada
  pinMode(PWM_PIN, OUTPUT);
  pinMode(MOTOR_PINS[0], OUTPUT);
  pinMode(MOTOR_PINS[1], OUTPUT);
  pinMode(ENCODER_PIN, INPUT);
  pinMode(RS, OUTPUT);
  pinMode(E, OUTPUT);
  for (int i=0; i<DISPLAY_VEC_LEN; i++) {
    pinMode(DATA[i], OUTPUT);
  }

  // Inicializa serial
  Serial.begin(9600);

  // Aloca espaço para a variável de comando
  cmdString.reserve(8);

  // Adiciona interrupção externa
  attachInterrupt(digitalPinToInterrupt(ENCODER_PIN), doCountObstruct, RISING);

  // Inicializa a interface com a tela LCD e especifica as dimensões (largura e altura) da tela
  lcd.begin(16, 2);

  // Inicializa comunicação serial
  Wire.begin();

  // Habilita interrupção no Timer0
  configTimer0();
  sei();
}

// Ciclo de vida
void loop()
{
  _delay_ms(1);
  if (Serial.available() > 0)
    doReadOneChar();
  if (decode)
    doDecode();
  if (elapsedTime >= SPEED_CALCULATION_PERIOD) {
    doCalculateSpeed();
  }
  if (hasSpeedEstimative) {
    doChangeSpeedEstimative();
  }
  if (setMotor) {
    doSetMotor();
    doUpdateLCD();
  }
  doSetDisplays();
}
