/*
Projeto 2
Rafael Gonçalves (RA: 186062)
Thais Del Picchia (RA:206043)
*/

// Constantes para os cálculos
const int SPEED_CALCULATION_PERIOD = 500;
const int PULSES_PER_ROTATION = 60;

// Variável para registro de tempo
volatile unsigned long elapsedTime = 0;

// Contador para calcular a velocidade do motor
volatile unsigned int obstructionCounter = 0;

// Variável com velocidade atual do motor
unsigned int currentSpeed = 0;

// Configuração Timer0
void configTimer0(){
  TCCR0A = 0x02;
  OCR0A = 124;
  TIMSK0 = 0x02;
  TCCR0B = 0x05;
}

// Rotina de servico de interrupcao do temporizador
ISR(TIMER0_COMPA_vect){
  elapsedTime += 8;
}

void doCountObstruct() {
  obstructionCounter++;
};

void doCalculateSpeed() {
  // velocidade = (numero de voltas)/(tempo em min) =
  //            = (número de pulsos/pulsos por rotação)/(tempo em ms/60000) = 
  //            = (60000*número de pulsos)/(pulsos por rotação*tempo em ms)
  currentSpeed = (60000*obstructionCounter)/(PULSES_PER_ROTATION*elapsedTime);
  obstructionCounter = 0;
  elapsedTime = 0;
};

// Configurações iniciais
void setup()
{
  // Inicializa serial
  Serial.begin(9600);
  
  // Adiciona interrupção externa
  attachInterrupt(digitalPinToInterrupt(2), doCountObstruct, RISING);
  
  configTimer0();
  sei();
}

// Ciclo de vida
void loop()
{
  _delay_ms(1);
  if (elapsedTime >= SPEED_CALCULATION_PERIOD) {
    doCalculateSpeed();

  	Serial.print(currentSpeed);
    Serial.println(" RPM");
  }
}

