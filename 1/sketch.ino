// Rafael Gonçalves (RA: 186062)

// Leds dos semáforos
enum semaphoreStates {OFF, GREEN, YELLOW, RED};

enum daySemaphoreStates {
    GREEN_FOR_CARS,
    WAIT_BEFORE_YELLOW,
    YELLOW_FOR_CARS,
    WAIT_BEFORE_RED,
    RED_FOR_CARS,
    WAIT_BEFORE_GREEN,
    DISABLED
};

int semaphoreState;
const int TIME_IN_YELLOW = 1000;

int carSemaphoreState;
int peopleSemaphoreState;

const int carSemaphoreRed = A1;
const int carSemaphoreYellow = A2;
const int carSemaphoreGreen = A3;

const int peopleSemaphoreRed = A4;
const int peopleSemaphoreGreen = A5;

// Sensor de luminosidade
const int ldrPin = A0;
const int LDR_TRESH = 88; // Valor com a luminosidade do simulador na metade
int ldrValue;
int calculateLdr;

const int NUM_READINGS = 10;

int readings[NUM_READINGS];
int readIndex = 0;
int total = 0;

// Tempo
unsigned long elapsedTime = 0;

// Botão
const int btnPin = 6;
int currentBtnValue;
int previousBtnValue;
int btnState;

// Display
enum countdownStates {
    ZERO,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    BEGIN,
    END,
    BLINK,
    STAND_BY
};

int carCountdownState;
int peopleCountdownState;

enum displayIndex {
    PEOPLE_DISPLAY = 4,
    CAR_DISPLAY
};

const int displayPinVec[] = {8, 9, 10, 11};
const int DISPLAY_VEC_LEN = 4;

// Função para limpar displays
void clearDisplay(int isForCar) {
    if (isForCar) {
        digitalWrite(CAR_DISPLAY, HIGH);
    } else {
        digitalWrite(PEOPLE_DISPLAY, HIGH);
    }
};

// Função para atualizar displays com um certo dígito
void setDisplay(int isForCar, int digit) {
    digitalWrite(displayPinVec[0], digit % 2 ? HIGH : LOW);
    digitalWrite(displayPinVec[1], (digit/2) % 2 ? HIGH : LOW);
    digitalWrite(displayPinVec[2], (digit/4) % 2 ? HIGH : LOW);
    digitalWrite(displayPinVec[3], (digit/8) % 2 ? HIGH : LOW);

    digitalWrite(CAR_DISPLAY, isForCar ? LOW : HIGH);
    digitalWrite(PEOPLE_DISPLAY, isForCar ? HIGH : LOW);
}

// Função para atualizar o semáforo de carros com base na variável de estado
void setCarSemaphore() {
  switch (carSemaphoreState) {
    case RED:
        digitalWrite(carSemaphoreRed, HIGH);
        digitalWrite(carSemaphoreYellow, LOW);
        digitalWrite(carSemaphoreGreen, LOW);
        break;
    case YELLOW:
        digitalWrite(carSemaphoreRed, LOW);
        digitalWrite(carSemaphoreYellow, HIGH);
        digitalWrite(carSemaphoreGreen, LOW);
        break;
    case GREEN:
        digitalWrite(carSemaphoreRed, LOW);
        digitalWrite(carSemaphoreYellow, LOW);
        digitalWrite(carSemaphoreGreen, HIGH);
        break;
    case OFF:
        digitalWrite(carSemaphoreRed, LOW);
        digitalWrite(carSemaphoreYellow, LOW);
        digitalWrite(carSemaphoreGreen, LOW);
  }
}

// Função para atualizar o semáforo de pessoas com base na variável de estado
void setPeopleSemaphore() {
  switch (peopleSemaphoreState) {
    case RED:
        digitalWrite(peopleSemaphoreRed, HIGH);
        digitalWrite(peopleSemaphoreGreen, LOW);
        break;
    case GREEN:
        digitalWrite(peopleSemaphoreRed, LOW);
        digitalWrite(peopleSemaphoreGreen, HIGH);
        break;
    case OFF:
        digitalWrite(peopleSemaphoreRed, LOW);
        digitalWrite(peopleSemaphoreGreen, LOW);
  }
}

// Máquina de estados finitos principal para o dia
void dayFsm() {
    switch (semaphoreState) {
        case GREEN_FOR_CARS:
            carSemaphoreState = GREEN;
            peopleSemaphoreState = RED;
            if (btnState == 1) {
                semaphoreState = WAIT_BEFORE_YELLOW;
                elapsedTime = 0;
            }
            break;
        case WAIT_BEFORE_YELLOW:
            if (elapsedTime >= 100) {
                semaphoreState = YELLOW_FOR_CARS;
            }
            break;
        case YELLOW_FOR_CARS:
            semaphoreState = WAIT_BEFORE_RED;
            carSemaphoreState = YELLOW;
            elapsedTime = 0;
            break;
        case WAIT_BEFORE_RED:
            if (elapsedTime >= TIME_IN_YELLOW) {
                semaphoreState = RED_FOR_CARS;
                peopleSemaphoreState = GREEN;
            }
            break;
        case RED_FOR_CARS:
            carSemaphoreState = RED;
            semaphoreState = WAIT_BEFORE_GREEN;
            elapsedTime = 0;
            break;
        case WAIT_BEFORE_GREEN:
            carCountdownState = BEGIN;
            peopleCountdownState = BEGIN;
            semaphoreState = DISABLED;
            break;
        default:
            break;
    }
}

// Máquina de estados finitos para a noite
void nightFsm() {
    if (elapsedTime >= 500) {
        elapsedTime = 0;
        if (carSemaphoreState == OFF) {
            carSemaphoreState = YELLOW;
            peopleSemaphoreState = RED;
        } else {
            carSemaphoreState = OFF;
            peopleSemaphoreState = OFF;
        }
    }
}

// Função que desenha e limpa displays alternadamente
void setDisplays() {
    if ((elapsedTime / 16) % 2) {
        // Display de carros
        if (carCountdownState == END || carCountdownState == STAND_BY)
           clearDisplay(true);
        else
            setDisplay(true, carCountdownState);
    } else {
        // Display de pessoas
        if (peopleCountdownState == END || peopleCountdownState == BLINK
                || peopleCountdownState == STAND_BY)
           clearDisplay(false);
        else if (peopleCountdownState == BEGIN)
            setDisplay(false, 5);
        else
            setDisplay(false, peopleCountdownState);
    }
}

// Máquina de estados finitos auxiliar para a contagem regressiva
void countdownFsm() {
    switch (carCountdownState) {
        case BEGIN:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = EIGHT;
                peopleCountdownState = FOUR;
            }
            break;
        case EIGHT:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = SEVEN;
                peopleCountdownState = THREE;
            }
            break;
        case SEVEN:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = SIX;
                peopleCountdownState = TWO;
            }
            break;
        case SIX:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = FIVE;
                peopleCountdownState = ONE;
            }
            break;
        case FIVE:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = FOUR;
                peopleCountdownState = BLINK;
            }
            break;
        case FOUR:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = THREE;
                peopleCountdownState = BLINK;
            }
            else if (elapsedTime >= 500) {
                peopleCountdownState = ZERO;
            }
            break;
        case THREE:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = TWO;
                peopleCountdownState = BLINK;
            }
            else if (elapsedTime >= 500) {
                peopleCountdownState = ZERO;
            }
            break;
        case TWO:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = ONE;
                peopleCountdownState = BLINK;
            }
            else if (elapsedTime >= 500) {
                peopleCountdownState = ZERO;
            }
            break;
        case ONE:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = ZERO;
                peopleCountdownState = BLINK;
            }
            else if (elapsedTime >= 500) {
                peopleCountdownState = ZERO;
            }
            break;
        case ZERO:
            if (elapsedTime >= 1000) {
                elapsedTime = 0;
                carCountdownState = END;
                peopleCountdownState = END;
            }
            else if (elapsedTime >= 500) {
                peopleCountdownState = ZERO;
            }
            break;
            break;
        case END:
            semaphoreState = GREEN_FOR_CARS;
            carCountdownState = STAND_BY;
            peopleCountdownState = STAND_BY;
            break;
        default:
            break;
    }
};

// Configurações iniciais
void setup() {
  Serial.begin(9600);

  pinMode(ldrPin, INPUT);
  pinMode(btnPin, INPUT);

  pinMode(carSemaphoreRed, OUTPUT);
  pinMode(carSemaphoreYellow, OUTPUT);
  pinMode(carSemaphoreGreen, OUTPUT);

  pinMode(peopleSemaphoreRed, OUTPUT);
  pinMode(peopleSemaphoreGreen, OUTPUT);

  pinMode(CAR_DISPLAY, OUTPUT);
  pinMode(PEOPLE_DISPLAY, OUTPUT);

  for (int i=0; i< DISPLAY_VEC_LEN; i++)
      pinMode(displayPinVec[i], OUTPUT);

  ldrValue = analogRead(ldrPin);
  for (int i = 0; i < NUM_READINGS; i++)
    readings[i] = ldrValue;
  total = NUM_READINGS * ldrValue;

  calculateLdr = true;
  carSemaphoreState = OFF;
  peopleSemaphoreState = OFF;
  semaphoreState = GREEN_FOR_CARS;
  previousBtnValue = LOW;
  carCountdownState = STAND_BY;
  peopleCountdownState = STAND_BY;
  clearDisplay(false);
  clearDisplay(true);

  configuracao_Timer0();
  sei();
}

// Ciclo de vida
void loop() {

  // Espera ativa acelera simulador
  _delay_ms(1);

  currentBtnValue = digitalRead(btnPin);
  // Debounce do botão via software
  if ((previousBtnValue == LOW) && (currentBtnValue == HIGH))
      btnState = 1;
  else
      btnState = 0;
  previousBtnValue = currentBtnValue;

  if (calculateLdr) {
      // Média móvel para filtrar ruído
      // Baseado em: https://www.arduino.cc/en/Tutorial/BuiltInExamples/Smoothing
      total = total - readings[readIndex];
      readings[readIndex] = analogRead(ldrPin);
      total = total + readings[readIndex];
      readIndex = (readIndex + 1) % NUM_READINGS;
      ldrValue = total / NUM_READINGS;
      calculateLdr = false;
  }

  setCarSemaphore();
  setPeopleSemaphore();
  setDisplays();

  if (ldrValue > LDR_TRESH) {
      // É noite
      nightFsm();
      // Importante para recomeçar do início ao voltar para a configuração "dia"
      semaphoreState = GREEN_FOR_CARS;
      carCountdownState = STAND_BY;
      peopleCountdownState = STAND_BY;
  } else {
      // É dia
      dayFsm();
      countdownFsm();
  }
  // DEBUG
  // Serial.println(ldrValue);
  // Serial.println(elapsedTime);
  // Serial.println(btnState);
}

void configuracao_Timer0(){
  ///////////////////////////////////////////////////////////////////////////
  // Configuracao Temporizador 0 (8 bits) para gerar interrupcoes periodicas a
  // cada 8ms no modo Clear Timer on Compare Match (CTC)
  // Relogio = 16e6 Hz
  // Prescaler = 1024
  // Faixa = 125 (contagem de 0 a OCR0A = 124)
  // Intervalo entre interrupcoes:
  // (Prescaler/Relogio)*Faixa = (64/16e6)*(124+1) = 0.008s

  // TCCR0A – Timer/Counter Control Register A
  // COM0A1 COM0A0 COM0B1 COM0B0 – – WGM01 WGM00
  // 0      0      0      0          1     0
  TCCR0A = 0x02;

  // OCR0A – Output Compare Register A
  OCR0A = 124;

  // TIMSK0 – Timer/Counter Interrupt Mask Register
  // – – – – – OCIE0B OCIE0A TOIE0
  // – – – – – 0      1      0
  TIMSK0 = 0x02;

  // TCCR0B – Timer/Counter Control Register B
  // FOC0A FOC0B – – WGM02 CS02 CS01 CS0
  // 0     0         0     1    0    1
  TCCR0B = 0x05;
  ///////////////////////////////////////////////////////////////////////////
}

// Rotina de servico de interrupcao do temporizador
ISR(TIMER0_COMPA_vect){
  elapsedTime += 8;
  calculateLdr = true;
}
