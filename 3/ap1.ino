#include <Wire.h>

// Endereço da primeira página da EEPROM
const unsigned char ADDR = 0x50;

void setup()
{
  Wire.begin();
  Serial.begin(9600);
}

// Escreve o byte data no endereço address na página page
void write2Eeprom(
  unsigned char address,
  unsigned char data
  )
{
  unsigned char page = (address & 0b11100000)>4;

  Wire.beginTransmission(ADDR + page);
  Wire.write(address);
  Wire.write(data);
  Wire.endTransmission();
  delay(5);
}

// Le o byte data do endereço address na página page
unsigned char readFromEeprom(unsigned char address)
{
  unsigned char data;
  unsigned char page = (address & 0b11100000)>4;

  Wire.beginTransmission(ADDR+page);
  Wire.write(address);
  Wire.endTransmission();

  Wire.requestFrom(ADDR + page, 1);
  if (Wire.available()){
     data = Wire.read();
  }

  return data;
}

void loop()
{
  char buf[12];
  unsigned char data;
  for (int i=0; i<10; i++)
    write2Eeprom(i,i);
  for (int i=0; i<10; i++) {
    data = readFromEeprom(i);
    Serial.println(itoa(data, buf, 10));
  }
  delay(1000);
}
