/*
Projeto 3
Rafael Gonçalves (RA: 186062)
Thais Del Picchia (RA:206043)
*/

// Importação de bibliotecas
#include <LiquidCrystal.h>
#include <Wire.h>

// Endereço da primeira página da EEPROM
const unsigned char MEM_ADDR = 0x50;

// Pinos do display
const int RS = 12;
const int E = 11;
const int DATA[] = {6,7,8,9};
const int DISPLAY_VEC_LEN = 4;

// Qual display está aceso
enum displayStates {
  ONE   = 0b11100000,
  TWO   = 0b11010000,
  THREE = 0b10110000,
  FOUR  = 0b01110000,
};

// Digitos a serem mostrados no display
int displayValues[4];

// Valor atual do sensor
int sensorValue;

// Confirmação do comando
bool confirmed;

// Gravar dados do sensor
bool storeValues = false;

// Comandos
enum commandStates {
  NONE,
  RESET,
  STATUS,
  BEGIN,
  STOP,
  TRANSFER,
  READ_NUMBER
};

int commandState;

// Flag para decodificar estado em ação
bool decodeCmd = false;

// Número de medidas a serem transferidas
int numberOfReadings = 0;

// LCD
//Definir os pinos do arduino conectados ao LCD, sendo a ordem:
// (rs, enable, entrada de dados)
LiquidCrystal lcd(RS,E,DATA[0],DATA[1],DATA[2],DATA[3]);

// Variável para registro de tempo
volatile unsigned long elapsedTime = 0;
volatile unsigned long pressTime = 0;
volatile unsigned long sensorTime = 0;

// Intervalo de leitura do sensor
const int SENSOR_READING_TIME = 2000;

// Expansor de portas
const int EXPANSOR_ADDR = 0x20;

// Pinos do teclado
constexpr int ROWS[] = {2, 3, 4, 5};
constexpr int COLS[] = {A0, A1, A2};

// Tempo de varredura das colunas do teclado
const int KEYPAD_POOLING_TIME = 50;

// Estados da varredura de colunas do teclado
enum KEYPAD_STATES {
  LEFT,
  CENTER,
  RIGHT,
};

int keypadState;

// Debounce
bool debounce = false;
const int DEBOUNCE_TIME = 30;

// Leitura de teclas
bool readKey = false;
int rowPressed;
int columnPressed;

// Pino do sensor
const int SENSOR = A3;

// Configuração Timer0
void configTimer0(){
  TCCR0A = 0x02;
  OCR0A = 124;
  TIMSK0 = 0x02;
  TCCR0B = 0x05;
}

// Rotina de servico de interrupcao do temporizador
ISR(TIMER0_COMPA_vect){
  elapsedTime += 8;
  pressTime += 8;
  sensorTime += 8;
}

// Função para mostrar valor nos displays de 7-seg usando mecanismo de
// multiplexação com ciclo total menor do que 1/30s
void doSetDisplays() {
    // elapsedTime / 8 => intervalos de 8ms
    // ciclos de 4*8 = 32ms < 1/30 s
    // (elapsedTime/8) % 4)
    switch ((elapsedTime>>3) & 0b00000011) {
      case 0:
        setDisplay(displayValues[0], ONE);
        break;
      case 1:
        setDisplay(displayValues[1], TWO);
        break;
      case 2:
        setDisplay(displayValues[2], THREE);
        break;
      case 3:
        setDisplay(displayValues[3], FOUR);
        break;
    }
}

// Função para atualizar display (display) com um certo dígito (digit)
void setDisplay(int  digit, int display) {
    byte data = 0x00;
    data = display | (digit&0x0F);
    Wire.beginTransmission(EXPANSOR_ADDR);
    Wire.write(data);
    Wire.endTransmission();
}

// Função para limpar display de 7-seg (usado apenas para debug)
void clearDisplays() {
    Wire.beginTransmission(EXPANSOR_ADDR);
    Wire.write(0xFF);
    Wire.endTransmission();
}

// Configurações iniciais
void setup()
{
  // Configura pinos
  pinMode(RS, OUTPUT);
  pinMode(E, OUTPUT);
  pinMode(SENSOR, INPUT);

  for (int i=0; i<DISPLAY_VEC_LEN; i++) {
    pinMode(DATA[i], OUTPUT);
  }

  for (int i=0; i<4; i++)
    pinMode(ROWS[i], INPUT_PULLUP);

  for (int i=0; i<3; i++)
    pinMode(COLS[i], OUTPUT);

  // Inicializa serial
  Serial.begin(9600);

  // Inicializa a interface com a tela LCD e especifica as dimensões (largura e altura) da tela
  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.write("BEM-VINDO");
  lcd.setCursor(0,1);
  lcd.write("ESCOLHA FUNCAO");

  // Inicializa comunicação TWI
  Wire.begin();

  // Referência para ADC de 1.1V
  analogReference(INTERNAL);

  // Habilita interrupções externas
  for (int i=0; i<4; i++)
      pciSetup(ROWS[i]);

  // Valores iniciais
  keypadState = LEFT;

  // Habilita interrupção no Timer0
  configTimer0();
  sei();
}

// Baseado em: https://playground.arduino.cc/Main/PinChangeInterrupt/
void pciSetup(byte pin)
{
    *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // habilita pin
    PCIFR  |= bit (digitalPinToPCICRbit(pin)); // remove outras interrupçÕes
    PCICR  |= bit (digitalPinToPCICRbit(pin)); // habilita interrupção pin change
}

// Interrupções de pin change para setar valores na matriz do teclado
ISR (PCINT2_vect) {
  int val;
  // Checa D2-D4
  for (int i=0; i<4; i++) {
    val = digitalRead(ROWS[i]);
    // Borda de descida
    if (val == LOW) {
      rowPressed = ROWS[i];
      columnPressed = keypadState;
      pressTime = 0;
      debounce = true;
      break;
    }
  }
}

// Leitura de tecla e atribuição de valores e estados
void doReadKey() {
  readKey = false;
  switch (rowPressed) {
    case ROWS[0]:
      switch (columnPressed) {
        case LEFT:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 1;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          } else {
            commandState = RESET;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.write("1-RESET");
            lcd.setCursor(0,1);
            lcd.write("CONFIRMAR?");
          }
          break;
        case CENTER:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 2;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          } else {
            commandState = STATUS;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.write("2-STATUS");
            lcd.setCursor(0,1);
            lcd.write("CONFIRMAR?");
          }
          break;
        case RIGHT:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 3;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          } else {
            commandState = BEGIN;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.write("3-START");
            lcd.setCursor(0,1);
            lcd.write("CONFIRMAR?");
          }
          break;
        default:
          break;
      }
      break;
    case ROWS[1]:
      switch (columnPressed) {
        case LEFT:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 4;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          } else {
            commandState = STOP;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.write("4-STOP");
            lcd.setCursor(0,1);
            lcd.write("CONFIRMAR?");
          }
          break;
        case CENTER:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 5;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          } else {
            commandState = TRANSFER;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.write("5-TRANSF DADOS:");
            lcd.setCursor(0,1);
            lcd.write("CONFIRMAR?");
          }
          break;
        case RIGHT:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 6;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          }
          break;
        default:
          break;
      }
      break;
    case ROWS[2]:
      switch (columnPressed) {
        case LEFT:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 7;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          }
          break;
        case CENTER:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 8;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          }
          break;
        case RIGHT:
          if (commandState == READ_NUMBER) {
            numberOfReadings = numberOfReadings * 10;
            numberOfReadings = numberOfReadings + 9;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          }
          break;
        default:
          break;
      }
      break;
    case ROWS[3]:
      switch (columnPressed) {
        case LEFT:
          if (commandState != NONE)
            commandState = NONE;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.write("CANCELADO");
            lcd.setCursor(0,1);
            lcd.write("ESCOLHA FUNCAO");
          break;
        case CENTER:
          if (commandState == READ_NUMBER)
            numberOfReadings = numberOfReadings * 10;
            lcd.setCursor(11,1);
            lcd.print(numberOfReadings);
          break;
        case RIGHT:
          if (commandState != NONE)
            confirmed = true;
          break;
        default:
          break;
      }
      break;
    }
  // DEBUG
  // Serial.println("KEYPRESS");
  // Serial.println(numberOfReadings);
  // Serial.println(commandState);
  // Serial.println(confirmed);
}

// Le tecla com debounce
void doDebounce() {
  debounce = false;
  int val = digitalRead(rowPressed);
  if (val == HIGH)
    readKey = true;
}

// Transfere dados via serial
void doTransfer(int n) {
  unsigned char counter = readCounter();
  unsigned char val;
  for (int i=1; i<=n; i++) {
    val = 256*readFromEeprom(2*(counter - i)) + readFromEeprom(2*(counter - i)+1);
    Serial.println(val);

    // DEBUG
    // Serial.print("pos: ");
    // Serial.println(2*(counter-i));
    // Serial.print("val: ");
    // Serial.println(val);
    // Serial.println(256*readFromEeprom(2*(counter - i)));
    // Serial.println(readFromEeprom(2*(counter - i)+1));
  };
}

// Escreve valor do contador
void writeCounter(unsigned char n) {
  write2Eeprom(2046, n/256);
  write2Eeprom(2047, n%256);
}

// Le contador
unsigned char readCounter() {
  return 256*readFromEeprom(2046) + readFromEeprom(2047);
}

// Escreve valor do sensor na eeprom via twi
void writeSensorValue() {
  unsigned char counter = readCounter();
  write2Eeprom(2*counter, sensorValue/256);
  write2Eeprom(2*counter+1, sensorValue%256);
  writeCounter(counter+1);

  // DEBUG
  // Serial.println("WRITE");
  // Serial.print("pos: ");
  // Serial.println(2*counter);
  // Serial.print("val: ");
  // Serial.print(sensorValue/256);
  // Serial.println(sensorValue%256);
}

// Escreve o byte data no endereço address na página page
void write2Eeprom(
  unsigned int address,
  unsigned char data
  )
{
  unsigned char page = (address>>8 & 0b11100000)>>4;

  Wire.beginTransmission(MEM_ADDR + page);
  Wire.write(address & 0xFF);
  Wire.write(data);
  Wire.endTransmission();
  _delay_ms(10);
}

// Le o byte data do endereço address na página page
unsigned char readFromEeprom(unsigned int address)
{
  unsigned char data;
  unsigned char page = (address>>8 & 0b11100000)>>4;

  Wire.beginTransmission(MEM_ADDR+page);
  Wire.write(address & 0xFF);
  Wire.endTransmission();

  Wire.requestFrom(MEM_ADDR + page, 1);
  if (Wire.available()){
     data = Wire.read();
  }

  return data;
}

// Muda coluna ativa
void doChangeKeypadState() {
  keypadState = (keypadState + 1) % 3;
  switch (keypadState) {
    case LEFT:
      digitalWrite(COLS[0], LOW);
      digitalWrite(COLS[1], HIGH);
      digitalWrite(COLS[2], HIGH);
      break;
    case CENTER:
      digitalWrite(COLS[0], HIGH);
      digitalWrite(COLS[1], LOW);
      digitalWrite(COLS[2], HIGH);
      break;
    case RIGHT:
      digitalWrite(COLS[0], HIGH);
      digitalWrite(COLS[1], HIGH);
      digitalWrite(COLS[2], LOW);
      break;
    default:
      break;
  }
}

void doReadSensor() {
  float temp;
  int adcRead = analogRead(SENSOR);
  temp = adcRead * (110/1023.);

  sensorValue = int(temp*10);

  // DEBUG
  //Serial.println(sensorValue);

  // Escreve valor no vetor que será mostrado pelos displays 7-seg
  displayValues[3] =   sensorValue % 10;
  displayValues[2] = ( sensorValue/10) % 10;
  displayValues[1] = ( sensorValue/100) % 10;
  displayValues[0] = ( sensorValue/1000) % 10;

  sensorTime = 0;

  // DEBUG
  /* Serial.println(temp); */
}

void doDecodeCmd() {
  switch (commandState) {
    case (RESET):
      writeCounter(0);
      commandState = NONE;
      confirmed = false;
      //Serial.println("RESET");
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.write("RESET OK");
      lcd.setCursor(0,1);
      lcd.write("ESCOLHA FUNCAO");
      break;
    case (STATUS):
      commandState = NONE;
      confirmed = false;
      //Serial.print("Dados gravados:");
      //Serial.println(readCounter());
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.write("GRAVADO: ");
      lcd.setCursor(9,0);
      lcd.print(readCounter());
      lcd.setCursor(0,1);
      lcd.write("DISPONIVEL: ");
      lcd.setCursor(11,1);
      lcd.print(1022 - readCounter());
      break;
    case (BEGIN):
      storeValues = true;
      commandState = NONE;
      confirmed = false;
      //Serial.println("BEGIN");
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.write("START");
      lcd.setCursor(0,1);
      lcd.write("INICIO COLETA");
      break;
    case (STOP):
      storeValues = false;
      commandState = NONE;
      confirmed = false;
      //Serial.println("STOP");
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.write("FIM DA COLETA");
      lcd.setCursor(0,1);
      lcd.write("N DADOS: ");
      lcd.setCursor(9,1);
      lcd.print(readCounter());
      break;
    case (TRANSFER):
      commandState = READ_NUMBER;
      confirmed = false;
      //Serial.println("TRANSFER");
      lcd.setCursor(0,1);
      lcd.write("ESC. QTDE: ");
      break;
    case (READ_NUMBER):
      if(numberOfReadings <= readCounter()){
          doTransfer(numberOfReadings);
          commandState = NONE;
          numberOfReadings = 0;
          confirmed = false;
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.write("TRANSFER OK ");
          lcd.setCursor(0,1);
          lcd.write("ESCOLHA FUNCAO ");
       }else{
          commandState = NONE;
          numberOfReadings = 0;
          confirmed = false;
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.write("QTDE INVALIDA ");
        }

      break;
  }
}

void loop()
{
  _delay_ms(1);
  if (elapsedTime > KEYPAD_POOLING_TIME)
    doChangeKeypadState();
  if (debounce && pressTime > DEBOUNCE_TIME)
    doDebounce();
  if (readKey)
    doReadKey();
  if (sensorTime > SENSOR_READING_TIME) {
    doReadSensor();
    if (storeValues)
      writeSensorValue();
  }
  if (commandState && confirmed) {
    doDecodeCmd();
  }
  doSetDisplays();
}
